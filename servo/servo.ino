#define S_SERVO 2

#define PERIOD 20000
#define LEFT 100
#define CENTER 1500
#define RIGHT 2500

void setup() {
  pinMode(S_SERVO, OUTPUT);
}

void loop() {
  position(RIGHT);
  position(CENTER);
  position(LEFT);
 
}


void position(int on){
  // The output has a 4n25 (PULLUP), so the high time and the low time has been stwiched
  int off = (PERIOD - on);

  for(int i = 0;i < 6; i ++){
    digitalWrite(S_SERVO, LOW);
    delayMicroseconds(on);
    digitalWrite(S_SERVO, HIGH);
    delayMicroseconds(off);  
  }
  delay(1000);
  
}
