#include "TM1637.h"
#define CLK 2//pins definitions for TM1637 and can be changed to other ports
#define DIO 3
int counter[] = {0, 0, 0, 0};
TM1637 tm1637(CLK, DIO);
void setup()
{
  tm1637.init();
  tm1637.set(BRIGHT_TYPICAL);//BRIGHT_TYPICAL = 2,BRIGHT_DARKEST = 0,BRIGHTEST = 7;
}

void loop() {
  delay(100);
  printDisplay();
}

void printDisplay() {
  if (!(counter [0] == 9 && counter [1] == 9  && counter [2] == 9  && counter [3] == 8)) {
    if (counter[3] != 9) {
      counter[3]++;
    } else {
      counter[3] = 0;
      if (counter[2] != 9) {
        counter[2]++;
      } else {
        counter[2] = 0;
        if (counter[1] != 9) {
          counter[1]++;
        } else {
          counter[1] = 0;
          if (counter[0] != 9) {
            counter[0]++;
          }
        }
      }
    }
  }

  tm1637.display(0, counter[0]);
  tm1637.display(1, counter[1]);
  tm1637.display(2, counter[2]);
  tm1637.display(3, counter[3]);
}
