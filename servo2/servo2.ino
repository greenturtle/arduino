#define S_SERVO 2
// GLOBAL VARIABLES
int pCenter  = 1500;
int pLeft    = 1000;
int pRight   = 2000;
int p = pCenter;
int cT2 = 0x00;
// FUNCTION
ISR(TIMER2_OVF_vect){
  cT2 += 1;

  if(cT2 == 20){
    cT2 = 0;

    digitalWrite(S_SERVO, HIGH);
    delayMicroseconds(p);
    digitalWrite(S_SERVO, LOW);

  }
}
// SETUP
void setup() {
  TCCR2A = 0xA3; // Faster PWM 
  TCCR2B = 0x04; // Pre scaler 1:64
  TIMSK2 = 0x01; // Enable Timer2 interruption
  sei(); // Enable global interruption
  pinMode(S_SERVO, OUTPUT);
}
// PROGRAM
void loop() {
  
}
